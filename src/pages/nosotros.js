import React from "react"
import Layout from '../components/layout';
import ContenidoNosotros from '../components/contenidoNosotros';


const Index = () => (
  <Layout>
    <ContenidoNosotros/>
  </Layout>
)

export default Index

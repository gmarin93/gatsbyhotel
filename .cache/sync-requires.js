const { hot } = require("react-hot-loader/root")

// prefer default export if available
const preferDefault = m => (m && m.default) || m


exports.components = {
  "component---src-components-habitaciones-js": hot(preferDefault(require("/home/sisagricola/Documents/Software/Gatsby/hotelgatsby/gatsbyhotel/src/components/habitaciones.js"))),
  "component---src-pages-404-js": hot(preferDefault(require("/home/sisagricola/Documents/Software/Gatsby/hotelgatsby/gatsbyhotel/src/pages/404.js"))),
  "component---src-pages-index-js": hot(preferDefault(require("/home/sisagricola/Documents/Software/Gatsby/hotelgatsby/gatsbyhotel/src/pages/index.js"))),
  "component---src-pages-nosotros-js": hot(preferDefault(require("/home/sisagricola/Documents/Software/Gatsby/hotelgatsby/gatsbyhotel/src/pages/nosotros.js")))
}

